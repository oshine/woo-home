p=`uname`
arch=`uname -m`
woo_name=""
if [ "$p" == "Linux" ] || [ "$p" == "Darwin" ]; then
    cd /usr/local && mkdir -p woo && cd woo
else
    # windows git bash
    cd ~/ && mkdir -p woo && cd woo
    curl "https://liverun.cc/i/iwin" -o woo-installer.exe
    powershell -Command ".\\woo-installer.exe"
    exit
fi

echo -e "\nChoose download mirror,1from China mainland,2from China HongKong(default)/请选择下载源，1中国大陆源，2中国香 港源(默认源)"
read -p "Please type/请输入:" mirror



function download() {
      real_url=$1
      echo "start download binary woo file from:${real_url}"

      curl -fSL "${real_url}" -H "referer: https://www.woolang.net" -o "${e}${woo_name}"
}


for e in `echo -e "woo\nwop"`;do

  if [ "$mirror" == "1" ]; then
      echo "download ${e} from mirror: China Mainland"
  else
      echo "download ${e} from mirror: China HongKong"
  fi

  url="https://liverun.cc/i/?p=${p}&arch=${arch}&v=${ver}&mirror=${mirror}&e=${e}"

  content=`curl -fSsL "${url}"`

  sha=`echo $content | cut -d "\n" -f 1`
  real_url=`echo $content | cut -d "\n" -f 2`

  if [ "$real_url" != "" ]; then
    echo "err: can not ready any data from url:${url}"
    exit
  fi

  if test -f "${e}${woo_name}"; then
      echo "already exist ${e}${woo_name},check sum..."
      current_sha=`sha1sum ${e}${woo_name}`
      if [ "$current_sha" != "$sha" ]; then
          download $real_url
      else
          echo "already installed with ${e},ignored"
      fi
  else
     download $real_url
  fi

done

dir=`./woo -r "_out(_pwd())"`

if [ "$p" == "Linux" ] || [ "$p" == "Darwin" ]; then
    chmod a+x woo
    chmod a+x wop

    curr_pwd=`pwd`
    ln -sf "${curr_pwd}/woo" /usr/local/bin/woo
    ln -sf "${curr_pwd}/wop" /usr/local/bin/wop
else
#    echo "attention please/请注意"
#
#    echo "there have not permission to set woo.exe to be default program to open .wop file / windows下无法在没有管理员权限时把wop.exe程序设置为默认打开.wop的程序，没有操作注册表权限"
#    echo "如果您已经使用管理员权限运行此安装命令，那么直接回车，否则请输入(y)来手动操作，程序将把注册表数据写到目录，然后您双击即可执行注册表导入，或者您可以下载windows的专用安装程序：https://liverun.cc/i/iwin.exe"
#    echo "如果此步骤失败，您将不能用直接用鼠标双击打开.wop文件，需要手动在控制台运行wop.exe *.wop"
#    r=read
#    if [ "$r"=="y" ]; then
#      echo "you choose manual,open folder now,double click [wop_reg.reg] file to finish register/您选择了手动操作，已经打开文件夹，双击[wop_reg.reg]文件完成注册表注册"
#
#      echo -e "Windows Registry Editor Version 5.00\n[HKEY_CLASSES_ROOT\\\.wop\\\shell\\\Open\\\command]\n@=\"${dir}\\\wop.exe %1\"" > wop_reg.reg
#      echo -e "[HKEY_CLASSES_ROOT\\\.wop\\\DefaultIcon]\n@=\"${dir}\\\wop.exe\"" >> wop_reg.reg
#
#      powershell -Command "explorer /select,wop_reg.reg"
#
#    fi
#    ./woo -r "print(woo.http:new():get('https://liverun.cc/i/env').str)" | ./woo -r
fi


echo "current install dir:${dir}"

