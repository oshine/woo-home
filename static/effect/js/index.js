
function show_write_panel() {
    window.open('/effect/edit', '_blank')
}

function go_code_edit(self) {
    window.open($(self).find('.main')[0].href, '_blank')
    show_write_panel()
    return false
}

new Vue({
    el: "#right_panel",
    data: {
        tags_list: [], // tag推荐列表
        member_active_list: [], // tag推荐列表
    },
    created: function () {
        // let member = localStorage.getItem('_member')
        // if (member) {
        //     this.member = JSON.parse(member)
        // }
        let self = this

        let data = cache.get('_talk_hot_tags')
        if (data) {
            self.tags_list = data
        } else {
            ajax('/talk/ajaxHotTag', 'POST', 'json', {}, null, function (res) {
                self.tags_list = res.data
                cache.set('_talk_hot_tags', res.data)
            }, null, true)
        }

        data = cache.get('_talk_active_member')
        if (data) {
            self.member_active_list = data
        } else {
            ajax('/talk/ajaxActiveMember', 'POST', 'json', {}, null, function (res) {
                self.member_active_list = res.data
                cache.set('_talk_active_member', res.data, 1600)
            }, null, true)
        }


    },
    computed: {},
    methods: {
        headClick() {
            this.is_notify_panel_show = false
            this.is_msg_panel_show = false
        }
    }
})

let tag_hide = $('.tag-hide')
for (let i = 0; i < tag_hide.length; i++) {
    if (tag_hide[i].innerHTML !== '') {
        tag_hide[i].style.display = 'inline-block'
    }
}

// effect

window.audio_play_url = ''
window.audio = document.createElement('audio') //生成一个audio元素
var ele = null

var duration

audio.addEventListener("canplay", function () {
    console.log("音频长度=>>>：", parseInt(audio.duration) + '秒', '音频时分秒格式：', (parseInt(audio.duration)));
    duration = audio.duration
});

$('.wav_view').on('click', function (e) {
    console.log(e)
    var url = $(e.target.parentElement).children('.wav_blend').attr('source')
    if (audio_play_url !== url) {
        if (audio_play_url == '') {
            play(null, url)
        } else {
            audio.pause()
            play(null, url)
        }
    }

    ele = e.target.parentElement.lastElementChild
    var len = e.offsetX
    audio.currentTime = duration * (len / 700)
    audio.play()
})

setInterval(function () {
    if (audio.paused) {
        return
    }
    var offset = audio.currentTime / duration * 700
    ele.style.width = offset + 'px'

}, 100)


function process(ele) {
    console.log(ele)
}

function play(e, src) {
    var url = e ? e.dataset.source : src
    if (audio.paused) {
        if (audio_play_url == '') {
            ele = $(e).parent().children('.wav_view').children('.wav_blend')[0]
            audio.src = url
            audio.play()
            audio_play_url = url
            return
        } else {
            if (audio_play_url != url) {
                audio.pause()
                audio.src = url
                audio.play()
                audio_play_url = url
            } else {
                audio.play()
            }
            return;
        }
    }
    audio.pause()
}



//分页

let page_name = 'index'
window.onload = function () {
    let tags = $('.tags')
    for (let i = 0; i < tags.length; i++) {
        let tag = tags[i]
        let jht = $(tag).html()
        console.log(jht)
        if (jht !== '') {
            let html = JSON.parse(jht)
            let tag_html = ''
            for (const item of html) {
                tag_html += '<a target="_blank" href="/tag/find?t=' + item.id + '">' + item.title + '  </a>'
            }
            $(tag).html(tag_html)
        }
    }

    // var tags = $('.cls_content')
    // tags.each(function () {
    //     var node = $(this)
    //     node.html(node.html().replace(/(#\S+ )/g, "<a href='/talk/tag?k=$1'>$1</a>"))
    // });

    let t = location.href
    let ind
    if (t.indexOf('x-slice') !== -1) {
        ind = '1'
        page_name = 'index-slice'
    } else {
        ind = '0'
    }
    $('li[data-index="' + ind + '"]').addClass('selected')
}

var current = new RegExp('\\?page=([0-9]*)')
var r = current.exec(location.href)
if (r && r[1]) {
    current = r[1]
} else {
    current = 1
}
$('#pagination').pagination({
    // pageCount: 10,
    totalData: page_count,
    showData: 10,
    jump: false,
    coping: false,
    current: current,
    homePage: '首页',
    prevContent: '上页',
    nextContent: '下页',
    callback: function (api) {
        // console.log(api, api.getCurrent())
        location.href = '/effect/' + page_name + '?page=' + api.getCurrent()
    }
});


function favorite(){
    if (!checkLogin()){
        showLoginPanel()
    }
    showAlertPanel('功能开发中')
}

function download(){
    if (!checkLogin()){
        showLoginPanel()
    }
    showAlertPanel('功能开发中')
}