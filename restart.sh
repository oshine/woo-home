#!/usr/bin/env bash

git pull

if [ "$1" == "1" ]
then
	ps -ef | grep ":process ooor.cc-async" | grep -v grep | awk '{print "kill -9 "$2}'|sh
	ps -ef | grep ":process ooor.cc-web" | grep -v grep | awk '{print "kill -9 "$2}'|sh

	pwd=`pwd`

	su - woo -c "cd $pwd && nohup ./murphy queue start :process ooor.cc-async> async.log 2>&1 &"

	su - woo -c "cd $pwd && nohup woo -t http :process ooor.cc-web> ooor.cc.log 2>&1 &"

else
	echo "reload"
	./murphy reload

fi
