#!/usr/bin/env bash

su - woo -c 'nohup ./murphy queue start :process liverun.cc-async> async.log 2>&1 &'

su - woo -c 'nohup woo -t http :process liverun.cc-web> liverun.cc.log 2>&1 &'
