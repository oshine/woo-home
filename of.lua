-- use socket to mock http instead of
-- "woo.http:new():get('http://www.baidu.com')"

local bts =
    _bytes(
    "GET / HTTP/1.1\r\nHost: www.baidu.com\r\nUser-Agent: curl/7.10\r\nConnection: Close\r\n\r\n"
)

local sock = woo.sockets:new()
local ok =
    sock:dial(
    "tcp",
    "www.baidu.com:80",
    function(ud, handler)
        r = handler:w(bts)
        local bytes, err = handler:r(1024)
        _out(bytes:str():reMatch("title>(.*?)</title")[1][2])
    end,
    2000
)
